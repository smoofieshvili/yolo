#!/usr/bin/env bash
BASE_DIR=/data/models
DATA_DIR=/data

# Start the training process
python biometrics_face/train.py
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start training process: $status"
  exit $status
fi

while [ 1 ]
do
  echo 'copy the weights mutterficker'
  sleep 60 &
  wait $!
done

# Start the tensorboard process
# tensorboard --logdir=/data/
# status=$?
# if [ $status -ne 0 ]; then
#   echo "Failed to start tensorboard: $status"
#   exit $status
# fi
