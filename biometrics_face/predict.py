from keras.models import load_model
import cv2
import argparse
import numpy as np
import os

argparser = argparse.ArgumentParser(
    description='predict raper')

argparser.add_argument(
    '-i',
    '--input',
    help='path to input image')


def main(args):
    image_path = args.input

    image = cv2.imread(image_path)
    predicted = predict(image)
    print('prediction = %s' % predicted)

def predict(image):
    image = cv2.resize(image, (224,224))
    image = np.expand_dims(image, axis=0)

    classes = sorted([name for name in os.listdir('biometrics_face/train/') if
                      name[0] != '.'])

    model = load_model('wundermodel.h5')

    predictions = model.predict(image)

    return classes[np.argmax(predictions)]

if __name__ == "__main__":
    main(argparser.parse_args()) 
