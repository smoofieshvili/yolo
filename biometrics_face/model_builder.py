from keras.layers import Input, Dense, Conv2D, MaxPool2D, Flatten, MaxPooling2D, Activation, Dropout, GlobalAveragePooling2D
from keras.models import Model, Sequential
from keras import applications

def build_mlp(input_shape, num_classes=10):
    inputs = Input(shape=input_shape)

    x = Dense(64, activation='relu')(inputs)
    x = Dense(64, activation='relu')(x)
    x = Dense(64, activation='relu')(x)

    predictions = Dense(num_classes, activation='softmax')(x)

    return Model(inputs=inputs, outputs=predictions)


def build_cnn(input_shape, num_classes=10):
    inputs = Input(shape=input_shape)

    x = Conv2D(32, (3, 3), padding='same', activation='relu')(inputs)
    x = Conv2D(32, (3, 3), padding='same', activation='relu')(x)
    x = MaxPool2D((2, 2))(x)  # (28, 28) -> (14, 14)

    x = Conv2D(64, (3, 3), padding='same', activation='relu')(x)
    x = Conv2D(64, (3, 3), padding='same', activation='relu')(x)
    x = MaxPool2D((2, 2))(x)  # (14, 14) -> (7, 7)

    x = Flatten()(x)  # 7*7 = 49
    x = Dense(128, activation='relu')(x)
    x = Dense(128, activation='relu')(x)
    predictions = Dense(num_classes, activation='softmax')(x)

    return Model(inputs=inputs, outputs=predictions)

def build_task1(input_shape, num_classes=4):
	#return applications.ResNet50(weights=None, include_top=True, input_shape=input_shape, classes=4)

	resnet_model = applications.ResNet50(weights='imagenet', include_top=False, input_shape=input_shape)

#	print(wunder_model)
#	print(wunder_model.output_shape)
	
	x = resnet_model.output
	x = GlobalAveragePooling2D()(x)
	x = Dense(128, activation='relu')(x)

	predictions = Dense(num_classes, activation='softmax')(x)

	return Model(inputs=resnet_model.input, outputs=predictions)

	model = Sequential()
	#for layer in resnet_model.layers:
	#	model.add(layer)

	model.add(Conv2D(32, (3, 3), input_shape=input_shape))
	model.add(Activation('relu'))
	model.add(MaxPooling2D(pool_size=(2, 2)))

	model.add(Conv2D(32, (3, 3)))
	model.add(Activation('relu'))
	model.add(MaxPooling2D(pool_size=(2, 2)))

	model.add(Conv2D(64, (3, 3)))
	model.add(Activation('relu'))
	model.add(MaxPooling2D(pool_size=(2, 2)))

	model.add(Flatten())
	model.add(Dense(128))
	model.add(Activation('relu'))
	
	model.add(Activation('relu'))
	model.add(Dense(num_classes))
	model.add(Activation('softmax'))

	return model
