FROM tensorflow/tensorflow:1.7.0-py3

RUN mkdir /data
WORKDIR /face
RUN ls /data

ENV PYTHONPATH=/face

EXPOSE 6006

COPY requirements.txt requirements.txt

RUN apt-get update && apt-get install -y libsm6 libfontconfig1 libxrender1 libxext6 locales locales-all
RUN pip install -r requirements.txt

# Set the locale
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8  


COPY run.sh .
COPY ./test/ ./test/
COPY ./biometrics_face/ ./biometrics_face/
COPY ./yolo/ ./yolo/
COPY weights.h5 .
COPY wundermodel.h5 .

#ENTRYPOINT ["./run.sh"]
