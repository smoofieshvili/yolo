DOCKER=docker
PORT=6006
JUPYTER_PORT=8888
VOLUME_PATH=volume_yolololo

IMAGE=pawelkobojek/biometrics
#IMAGE=gcr.io/tensorflow/tensorflow:latest-gpu
CONTAINER_NAME=biometrics

build:
	$(DOCKER) build $(ADDITIONAL_ARGS) -t $(IMAGE) .

run:
	$(DOCKER) run \
	--name=$(CONTAINER_NAME) \
	--rm \
	-p $(PORT):6006 \
	-it \
	-v $(VOLUME_PATH):/data \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE)


predict-class:
	#file - variable with path to image file
	$(eval file?=biometrics_face/train/ostr/0ec8926e-3054-11e8-9998-0242ac110002.png)
	$(DOCKER) run \
	--name=$(CONTAINER_NAME) \
	--rm \
	-p $(PORT):6006 \
	-it \
	-v $(VOLUME_PATH):/data \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE) \
	python biometrics_face/predict.py -i $(file) 
train-yolo:
	$(DOCKER) run \
	--name=$(CONTAINER_NAME) \
	--rm \
	-p $(PORT):6006 \
	-it \
	-v $(VOLUME_PATH):/data \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE) \
	python yolo/train.py -c yolo/config.json


predict-yolo:
	#file - variable with path to image file
	$(eval file?=/data/Kaczynski-kule.jpg)
	$(DOCKER) run \
	--name=$(CONTAINER_NAME) \
	--rm \
	-p $(PORT):6006 \
	-it \
	-v $(VOLUME_PATH):/data \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE) \
	python yolo/predict.py -c yolo/config.json -w /data/full_yolo_face.h5 -i $(file) 

.PHONY: build run
